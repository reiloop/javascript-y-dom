"use strict";

const url = "https://randomuser.me/api";

function devolverUser(num) {
  for (let i = 0; i < num; i++) {
    fetch(url) //hacemos fetch a la url
      .then((response) => response.json())
      .then((data) => {
        //creamos un array de usuarios con los datos de la api
        let usuarios = [
          { Nombre: data.results[0].name.first },
          { Apellido: data.results[0].name.last },
          { Género: data.results[0].gender },
          { Usuario: data.results[0].login.username },
          { País: data.results[0].location.country },
          { Email: data.results[0].email },
          { Foto: data.results[0].picture.large },
        ];
        console.table(usuarios); //Mostrar los datos del array
      })
      .catch((err) => console.log(err));
  }
}
//La función devolverUser devuelve tantos usuarios como se indica
devolverUser(2);
