"use strict";

// Escribe una función que, al recibir un array como parámetro,
//  elimine los strings repetidos del mismo.

// // No se permite hacer uso de Set ni Array.from().

const names = [
  "A-Jay",
  "Manuel",
  "Manuel",
  "Eddie",
  "A-Jay",
  "Su",
  "Reean",
  "Manuel",
  "A-Jay",
  "Zacharie",
  "Zacharie",
  "Tyra",
  "Rishi",
  "Arun",
  "Kenton",
];

function eliminarRepetidos(arreglo) {
  let result = []; //Creamos un nuevo array vacio
  arreglo.forEach((item) => {
    //Para cada item del array
    if (!result.includes(item)) {
      //miramos si no lo tenemos y lo añadimos; y si ya lo tenemos pues no
      result.push(item);
    }
  });
  //Mostramos array original
  console.log("Array con elementos repetidos:", arreglo);
  //Mostramos el array modificado
  console.log("Array sin elementos repetidos:", result);
}

eliminarRepetidos(names);
