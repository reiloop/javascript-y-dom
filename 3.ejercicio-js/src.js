"use strict";
// # Ejercicio 3

// Crea un programa que reciba un número en decimal o binario y devuelva la conversión:

// -   Si le pasamos un nº en decimal debe retornar la conversión a binario.

// -   Si le pasamos un nº en binario deberá devolver su equivalente decimal.

// Para ello la función deberá recibir un segundo parámetro que indique la base: 10 (decimal) o 2 (binario).

// No se permite utilizar el método parseInt().

//Función que convierte un string binario a decimal.
function binarioDecimal(num) {
  let numReverse = num.split("").reverse().join("");
  let i;
  let sum = 0;
  for (i = 0; i < numReverse.length; i++) {
    sum = sum + numReverse[i] * 2 ** i;
  }
  return sum;
}

//funcion que solicite números.
function pedirNumeros() {
  let num = Number(prompt("INTRODUCE EL NÚMERO A CONVERTIR"));
  //Solicitar dicho número

  const base = prompt(
    "Introduce la base en la que está el número: 10 -> decimal // 2 -> binario"
  ); //indicar si el número está en base 10 o en base 2.

  //dependiendo de que base se elija convertiremos a binario o a decimal
  switch (base) {
    case "10": //si el numero es base 10 se convierte a binario
      const numBinario = num.toString(2);
      alert(`El numero en binario: ${numBinario}`);
      break;
    case "2": //si el numero es base 2 se convierte a decimal
      const numDecimal = binarioDecimal(num.toString());
      alert(`El numero en decimal: ${numDecimal}`);
      break;

    default:
      //si se introduce la base incorrecta se da la opción de volver a introducirla.
      if (confirm("Base incorrecta, quiere volver a probar")) {
        return pedirNumeros();
      }
  }
}
//Inicio la función pedir numeros
pedirNumeros();
