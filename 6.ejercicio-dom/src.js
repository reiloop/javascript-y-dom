"use strict";

const ps = document.getElementsByTagName("p");

for (const p of ps) {
  //para cada uno de los párrafos
  let parrafoHtm = p.innerHTML; //convertimos la info del párrafo a texto
  let parrArr = parrafoHtm.split(/[\s,\.,\"]+/); //añadimos los elementos de la página a un array
  parrArr.forEach((wrd) => {
    //para cada palabra si es mayor que 5
    if (wrd.length > 5) {
      //le añadimos el subrayado insertando la etiqueta <u></u>
      parrafoHtm = parrafoHtm.split(wrd).join(`<u>${wrd}</u>`);
    }
  });
  p.innerHTML = parrafoHtm;
}
