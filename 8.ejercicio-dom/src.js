"use strict";
let c = 0;
let s = 0;
let m = 0;
let h = 0;
let intervalId;
//función que dá formato a los minutos o segundos menores de 10
function format(num) {
  if (num < 10) {
    return "0" + num;
  } //si los segundos, los minutos llegan a 60 deberán mostrarse como 00.
  if (num === 60) {
    return "00";
    num;
  }
  return num;
}
function format_C(num) {
  if (num < 10) {
    return "0" + num;
  }
  return num;
}
//Función crono que nos muestra el tiempo en hh:mm:ss:cc
function crono() {
  if (c === 100) {
    c = 0;
    s++;
    if (s === 60) {
      s = 0;
      m++;
      if (m === 60) {
        m = 0;
        h++;
      }
    }
  }
  let centesimas = format_C(c);
  let minutos = format(m);
  let segundos = format(s);
  let horas = format(h);
  c++;
  const cronometro = document.getElementById("cronometro");
  cronometro.textContent = `${horas}:${minutos}:${segundos}:${centesimas}`;
}

//función que hace que el crono comience a funcionar cada centesima con un setInterval
function iniciar() {
  intervalId = setInterval(crono, 10);
  document.getElementById("inicio").disabled = true; //desactivar el boton una vez iniciamos para no llamar a la funcion dos veces
  document.getElementById("parar").disabled = false; //aseguramos que el botón de parar esté activo
}
//función que para el cronometro
function parar() {
  clearInterval(intervalId);
  document.getElementById("inicio").disabled = false;
  document.getElementById("parar").disabled = true;
}
//función que devuelve todos los valores a cero y además para el cronometro.
function reset() {
  clearInterval(intervalId);
  s = 0;
  m = 0;
  h = 0;
  document.getElementById("cronometro").textContent = "00:00:00";
  document.getElementById("inicio").disabled = false;
  document.getElementById("parar").disabled = false;
}
