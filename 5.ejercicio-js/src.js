"use strict";

const urlEspisodios = "https://rickandmortyapi.com/api/episode";

fetch(urlEspisodios)
  .then((response) => response.json())
  .then((lista) => {
    const resultados = lista.results;
    for (let i = 0; i < resultados.length; i++) {
      const episodio = resultados[i]; //Buscar dentro de los episodios, los que tiene air date "January"
      if (episodio.air_date.includes("January")) {
        const personajes = episodio.characters; //Dentro de los que fueron lanzados en enero, sacar los personajes
        for (let i = 0; i < personajes.length; i++) {
          const link = personajes[i];
          fetch(link) //El array de personajes nos devulve los enlaces con la info así que hacemos un fetch para obtener el nombre
            .then((response) => response.json())
            .then((nombre) => {
              console.log(
                `Personaje: (${nombre.name}) /*****/ Fecha del episodio: (${episodio.air_date}) `
              );
            });
        }
      }
    }
  });
