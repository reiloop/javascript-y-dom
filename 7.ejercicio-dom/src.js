"use strict";

const add = document.querySelector("button");
const body = document.querySelector("main");
const handleAddClick = () => {
  const newItem = document.createElement("div");
  newItem.textContent = " ";
  body.appendChild(newItem);
  const random = (max) => Math.floor(Math.random() * max);

  setInterval(() => {
    newItem.style.backgroundColor = `rgb(${random(255)},${random(255)},${random(
      255
    )})`;
  }, 1000);
};

//Al hacer click se añade un nuevo Div que tambien cambia de color
add.addEventListener("click", handleAddClick);

/*tabla en la cual cada celula cambie el color de fondo*/
const divs = document.querySelectorAll("div");
const random = (max) => Math.floor(Math.random() * max);
for (const div of divs) {
  setInterval(() => {
    div.style.backgroundColor = `rgb(${random(255)},${random(255)},${random(
      255
    )})`;
  }, 1000);
}
