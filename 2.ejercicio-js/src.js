"use strict";
let m = 0;
let h = 0;
let d = 0;
let s = 0;

//función que dá formato a los minutos o segundos menores de 10
function format_sm(num) {
  if (num < 10) {
    return "0" + num;
  } //si los segundos, los minutos llegan a 60 deberán mostrarse como 00.
  if (num == 60) {
    return "00";
  }
  return num;
}
//función que dá formato a los número menores de 10
function format_horas(num) {
  if (num < 10) {
    return "0" + num;
  } //las 24 horas, deberán mostrarse como 00.
  if (num == 24) {
    return "00";
  }
  return num;
}
//funcion para formatear los días menores de 10.
function format_dias(num) {
  if (num < 10) {
    return "0" + num;
  }
  return num;
}

//función que actualiza el reloj cada 5 segundos y los va sumando de 5 en 5.
function actualizarReloj() {
  //si los segundos llegan a 60, se suma 1 minuto y los segundos vuelven a cero.
  if (s == 60) {
    s = 0;
    m++;
    //si los minutos llegan a 60, se suma 1 hora y los minutos vuelven a cero.
    if (m == 60) {
      m = 0;
      h++;
      //si las horas llegan a 24, se suma 1 dia y los horas vuelven a cero.
      if (h == 24) {
        h = 0;
        d++;
      }
    }
  }

  let horas = format_horas(h);
  let minutos = format_sm(m);
  let segundos = format_sm(s);
  let dias = format_dias(d);
  s = s + 5;
  //modificaremos el html para mostrar el tiempo
  const crono = document.getElementById("tiempo");
  crono.textContent = `Han transcurrido ${segundos} segundos ${minutos} minutos ${horas} horas y ${dias} días desde la ejecución`;
  console.log(
    `Han transcurrido ${segundos} segundos ${minutos} minutos ${horas} horas y ${dias} días desde la ejecución`
  );
  //mostrar el tiempo cada 5 segundos
  setTimeout(actualizarReloj, 5000);
}

actualizarReloj();
